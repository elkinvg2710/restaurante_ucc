/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cruds;

import connector.ConexionObjetos;
import java.sql.ResultSet;
import java.util.LinkedList;
import patronbuilder.BuilderPlatoAcompañamientos;
import patronbuilder.BuilderPlatoBebidas;
import patronbuilder.BuilderPlatoCarnes;
import patronbuilder.BuilderPlatoEntradas;
import patronbuilder.PlatoDirector;
import pojos.Plato;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class CRUDPlatos {
    
    private static ConexionObjetos conn;
    
    public CRUDPlatos(){
        try{
            conn = ConexionObjetos.getInstance();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public static LinkedList<Plato> listarPlatos(){
        LinkedList<Plato> listaPlatos = new LinkedList<>();
        
        try{
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM platos, category WHERE platos.category = category.id");
            while(rs.next()){
                Plato plato = new Plato();
                plato.setId(rs.getInt("id"));
                plato.setName(rs.getString("name"));
                plato.setDescription(rs.getString("description"));
                plato.setPrice(rs.getInt("price"));
                plato.setCategory(rs.getInt("category"));
                
                plato.setCategoryName(rs.getString("category.name"));
                
                listaPlatos.add(plato);
            }
            rs.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return listaPlatos;
    }
     public static LinkedList<Plato> listarPlatosBuilder(int categoryB) {
        LinkedList<Plato> listaPlatoEntradas = new LinkedList<>();
        LinkedList<Plato> listaPlatoCarnes = new LinkedList<>();
        LinkedList<Plato> listaPLatoBebidas = new LinkedList<>();
        LinkedList<Plato> listaPlatoAcompañamientos = new LinkedList<>();

        PlatoDirector pd = new PlatoDirector();

        try {
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM platos "
                    + " WHERE category = " + categoryB);

            while (rs.next()) {

                if (categoryB == 1) {
                    pd.setPlatoBuilder(new BuilderPlatoEntradas());
                    pd.BuildPlato(rs.getInt("id"),rs.getString("name"), rs.getString("description"),
                            rs.getInt("price"), categoryB);
                    listaPlatoEntradas.add(pd.getPlatoPatronBuilder());

                } else if (categoryB == 2) {
                    pd.setPlatoBuilder(new BuilderPlatoCarnes());
                    pd.BuildPlato(rs.getInt("id"),rs.getString("name"), rs.getString("description"),
                            rs.getInt("price"), categoryB);
                    listaPlatoCarnes.add(pd.getPlatoPatronBuilder());

                } else if (categoryB == 3) {
                    pd.setPlatoBuilder(new BuilderPlatoAcompañamientos());
                    pd.BuildPlato(rs.getInt("id"),rs.getString("name"), rs.getString("description"),
                            rs.getInt("price"), categoryB);
                    listaPlatoAcompañamientos.add(pd.getPlatoPatronBuilder());

                } else if (categoryB == 4) {
                    pd.setPlatoBuilder(new BuilderPlatoBebidas());
                    pd.BuildPlato(rs.getInt("id"),rs.getString("name"), rs.getString("description"),
                            rs.getInt("price"), categoryB);
                    listaPLatoBebidas.add(pd.getPlatoPatronBuilder());
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (categoryB == 1) {
            return listaPlatoEntradas;
        } else if (categoryB == 2) {
            return listaPlatoCarnes;
        } else if (categoryB == 3) {
            return listaPlatoAcompañamientos;
        } else if (categoryB == 4) {
            return listaPLatoBebidas;
        }
        return null;
    }
    
    public void insertarPlato(Plato plato){
        Plato nuevoPlato = new Plato();
        String name = plato.getName();
        String description = plato.getDescription();
        int price = plato.getPrice();
        int category = plato.getCategory();
        
        try{
            conn.getbConn().getSt().executeUpdate("INSERT INTO platos (name, description, price, category)"+
                    "values ('" + name + "', '"+description+" ', '"+price+"', '"+category+"')");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void modificarPlato(Plato plato){
        int id= plato.getId();
        String name = plato.getName();
        String description = plato.getDescription();
        int price = plato.getPrice();
        int category = plato.getCategory();
        
        try{
            conn.getbConn().getSt().executeUpdate("UPDATE platos SET name ='" + name +  "'," 
                    + "description =' " + description 
                    + "', price ='" + price 
                    + "', category = '" + category
                    + "' WHERE id = " + id );
            
        }catch (Exception e){
            
            e.printStackTrace();
        }
    }
    
    public void borrarPlato (int id){
        
        try{
            conn.getbConn().getSt().executeUpdate("DELETE FROM platos WHERE id =" + id);
        
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
}
