/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testPatrones;

import connector.ConexionObjetos;
import cruds.CRUDPlatos;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Plato;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class TestPatrones {
    
     static CRUDPlatos crudPlatos;
    public static void main(String args[]) {

        crudPlatos = new CRUDPlatos();
        listarPlatos();
        
        Plato platoNuevo = new Plato();
        platoNuevo.setName("consome");
        platoNuevo.setDescription("entrada");
        platoNuevo.setPrice(5000);
        platoNuevo.setCategory(1);

        crudPlatos.insertarPlato(platoNuevo);
        listarPlatos();
       
        Plato platoActualizado = new Plato();
       
        platoActualizado.setId(6);
        platoActualizado.setName("ensalada dulce");
        platoActualizado.setDescription("verduras");
        platoActualizado.setPrice(6000);
        platoActualizado.setCategory(1);
        
        crudPlatos.insertarPlato(platoActualizado);
        listarPlatos();
        
        crudPlatos.borrarPlato(3);
        listarPlatos();
        
        
    }
    
    public static void listarPlatos(){    
         LinkedList<Plato> listaPlatos = crudPlatos.listarPlatos();
            System.out.println("--------------------------------------");
        for (int x = 0; x < listaPlatos.size(); x++) {
            System.out.println("name --> " + listaPlatos.get(x).getName() + "/ price $ >>>> " + listaPlatos.get(x).getPrice());
            Plato plato = listaPlatos.get(x);
            System.out.println("description --> " + plato.getDescription() + "/ category  >>>> " + plato.getCategoryName());
        }
    }
    
}
