/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testPatrones;

import static cruds.CRUDPlatos.listarPlatos;
import java.util.LinkedList;
import patronfacade.FacadePlatos;
import patronfacade.PlatosFacade;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class TestPatronesFacade {
    
static FacadePlatos facadePlatos;

    public static void main(String args[]) {

     facadePlatos =  new FacadePlatos();
     listarPlatos();

    }

        public static void listarPlatos(){
        LinkedList<PlatosFacade> listaPlatosFacade = facadePlatos.generarPlatosFacade();
        System.out.println("\033[34m --------------------------------------------------------------------------- \u001B[0m");
        for (int x = 0; x < listaPlatosFacade.size(); x++) {
            System.out.println("name --> " + listaPlatosFacade.get(x).getNamePlato() + "/ price $ ---> " + 
                    listaPlatosFacade.get(x).getPricePlato());
            PlatosFacade plato = listaPlatosFacade.get(x);
            System.out.println("description --> " + plato.getDescriptionPlato() + "/ category  >>>> " + 
                    plato.getCategoryPlato());
        }
    }
}
