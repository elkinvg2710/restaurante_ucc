/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import cruds.CRUDPlatos;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pojos.Plato;




@WebServlet(name = "ControladorPlatos", urlPatterns = {"/ControladorPlatos"})
public class ControladorPlatos extends HttpServlet {

    CRUDPlatos crudPlatos;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        crudPlatos = new CRUDPlatos();

        int id = 0;
        if (!request.getParameter("id").equals("") && request.getParameter("id") != null) {
            id = Integer.parseInt(request.getParameter("id"));
        }
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        int price = Integer.parseInt(request.getParameter("price"));
        int category = Integer.parseInt(request.getParameter("category"));

        Enumeration<String> parameters = request.getParameterNames();
        parameters.nextElement();
        parameters.nextElement();
        parameters.nextElement();
        parameters.nextElement();
        parameters.nextElement();
        
        String operacion = parameters.nextElement();
        
        Plato plato = new Plato();
        plato.setId(id);
        plato.setName(name);
        plato.setDescription(description);
        plato.setPrice(price);
        plato.setCategory(category);
        
        if(operacion.equals("insertarplato")){
            crudPlatos.insertarPlato(plato);            
        }
        if(operacion.equals("editarplato")){
            crudPlatos.modificarPlato(plato);
        }
        if(operacion.equals("eliminarplato")){
            crudPlatos.borrarPlato(plato.getId());
            
        }   
        
        response.sendRedirect("platos.jsp");   
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorPlatos</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorPlatos at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
