/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronfacade;

import connector.ConexionObjetos;
import cruds.CRUDCategorias;
import cruds.CRUDPlatos;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Category;
import pojos.Plato;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class FacadePlatos {

    private CRUDPlatos crudPlatos;
    private CRUDCategorias crudCategorias;
    private ConexionObjetos conn;

    public FacadePlatos() {
        crudPlatos = new CRUDPlatos();
        crudCategorias = new CRUDCategorias();
        conn = new ConexionObjetos();
    }
// en este metodo debo generar la actividad que se va a proveer al cliente
// DEBO TENER CUIDADO CON LOS ID'S QUE SE PROPAGAN ENTRE OBJETOS.

    public LinkedList<PlatosFacade> generarPlatosFacade() {
        LinkedList<PlatosFacade> listaPlatosCategoria = new LinkedList<>();
        LinkedList<Plato> listaPlatos = crudPlatos.listarPlatos();
        LinkedList<Category> listaCategorias = crudCategorias.listarCategorias();
        /*
LinkedList<Category> listaCategorias = new LinkedList<>();
// LO IDEAL ES QUE LOS DATOS DE CADA TABLA LOS OBTENGA A TRAVÉS DE UN CRUD
// DONDE ESTÉ IMPLEMENTADO EL MÉTODO listarObjetos() -> crudCategorias.listarCategorias();
try{
ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM category");
// siempre que quiero tomar datos de un resultset debo llamar al metodo next()
while(rs.next()){
Category cat = new Category();
cat.setId(rs.getInt("id"));
cat.setName(rs.getString("name"));
listaCategorias.add(cat);
}
rs.close();
} catch (Exception e){
e.printStackTrace();
}
         */
        for (int x = 0; x < listaPlatos.size(); x++) {
            for (int y = 0; y < listaCategorias.size(); y++) {
                if (listaPlatos.get(x).getCategory() == listaCategorias.get(y).getId()) {
                    listaPlatos.get(x).setCategoryName(listaCategorias.get(y).getName());
                    System.out.println("id_categoria >>> " + listaPlatos.get(x).getCategory());
                    System.out.println("name_categoria >>> " + listaCategorias.get(y).getName());
                    System.out.println("--------------------");
                }
            }
            Plato p = listaPlatos.get(x);
            PlatosFacade pf = new PlatosFacade(p.getId(), p.getName(), p.getDescription(),
                    p.getPrice(), p.getCategoryName());
            listaPlatosCategoria.add(pf);
        }
        return listaPlatosCategoria;
    }
}
