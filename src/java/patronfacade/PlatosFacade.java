
package patronfacade;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class PlatosFacade {
   private int id;
    private String namePlato;
    private String descriptionPlato;
    private int pricePlato;
    private String categoryPlato;

    public PlatosFacade(int id, String namePlato, String descriptionPlato,
            int pricePlato, String categoryPlato) {
        this.id = id;
        this.namePlato = namePlato;
        this.descriptionPlato = descriptionPlato;
        this.pricePlato = pricePlato;
        this.categoryPlato = categoryPlato;
    }  

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamePlato() {
        return namePlato;
    }

    public void setNamePlato(String namePlato) {
        this.namePlato = namePlato;
    }

    public String getDescriptionPlato() {
        return descriptionPlato;
    }

    public void setDescriptionPlato(String descriptionPlato) {
        this.descriptionPlato = descriptionPlato;
    }

    public int getPricePlato() {
        return pricePlato;
    }

    public void setPricePlato(int pricePlato) {
        this.pricePlato = pricePlato;
    }

    public String getCategoryPlato() {
        return categoryPlato;
    }

    public void setCategoryPlato(String categoryPlato) {
        this.categoryPlato = categoryPlato;
    }
}
