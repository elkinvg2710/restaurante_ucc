/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronprototype;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class FacturaVentaDetal extends PrototipoFacturaPlato{
    
    private int precioAlDetal;
    private double resultadoIvaAlDetal;
    private double precioTotal;
    
    public FacturaVentaDetal(String tipodeAlimento, int contenidoGramos, int numeroFactura, int precioAlDetal, 
            double resultadoIvaDetal, double precioTotal) {

        super(tipodeAlimento, contenidoGramos, numeroFactura);
        this.precioAlDetal = precioAlDetal;
        this.resultadoIvaAlDetal = resultadoIvaDetal;
        this.precioTotal = precioTotal;
    }

    public int getPrecioAlDetal() {
        return precioAlDetal;
    }

    public void setPrecioAlDetal(int precioAlDetal) {
        this.precioAlDetal = precioAlDetal;
    }

    public double getResultadoIvaAlDetal() {
        return resultadoIvaAlDetal;
    }

    public void setResultadoIvaAlDetal(double resultadoIvaAlDetal) {
        this.resultadoIvaAlDetal = resultadoIvaAlDetal;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }
    
    
    
}
