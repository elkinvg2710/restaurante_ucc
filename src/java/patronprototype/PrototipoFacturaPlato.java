/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronprototype;

/**
 *
 * @author Elkin Valencia Gomez
 */
public abstract class PrototipoFacturaPlato implements Cloneable{
    
    private String tipodeAlimento;
    private int contenidoGramos;
    private int numeroFactura;
    
    public PrototipoFacturaPlato (String  tipoAlimento, int contenidoGramos, int numeroFactura ){
    this.tipodeAlimento = tipoAlimento;
    this.contenidoGramos = contenidoGramos;
    this.numeroFactura = numeroFactura;
    
    }

    public int getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(int numeroFactura) {
        this.numeroFactura = numeroFactura;
    }
    
    public Object Clone() throws CloneNotSupportedException{
        return super.clone();
    
    
    } 

    public String getTipodeAlimento() {
        return tipodeAlimento;
    }

    public void setTipodeAlimento(String tipodeAlimento) {
        this.tipodeAlimento = tipodeAlimento;
    }

    public int getContenidoGramos() {
        return contenidoGramos;
    }

    public void setContenidoGramos(int contenidoGramos) {
        this.contenidoGramos = contenidoGramos;
    }
    
}
