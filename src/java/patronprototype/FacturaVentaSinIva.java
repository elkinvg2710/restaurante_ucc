/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronprototype;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class FacturaVentaSinIva extends PrototipoFacturaPlato{
    
    private int precioSinIva;
    private int cantidadSinIva;
    private int montoTotalSinIva;

    public FacturaVentaSinIva(String tipodeAlimento, int contenidoGramos, int numeroFactura,
            int precioSinIva, int cantidadSinIva, int montodeToSinIva) {

        super(tipodeAlimento, contenidoGramos, numeroFactura);
        this.precioSinIva = precioSinIva;
        this.cantidadSinIva = cantidadSinIva;
        this.montoTotalSinIva = montoTotalSinIva;
    }

    public int getPrecioSinIva() {
        return precioSinIva;
    }

    public void setPrecioSinIva(int precioSinIva) {
        this.precioSinIva = precioSinIva;
    }

    public int getCantidadSinIva() {
        return cantidadSinIva;
    }

    public void setCantidadSinIva(int cantidadSinIva) {
        this.cantidadSinIva = cantidadSinIva;
    }

}
