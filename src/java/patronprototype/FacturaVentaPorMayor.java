/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronprototype;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class FacturaVentaPorMayor extends PrototipoFacturaPlato{
    
    private int precioPorMayor;
    private int cantidad;
    private int montoTotal;
    private double montoTotalConIva;

    public FacturaVentaPorMayor(String tipodeAlimento, int contenidoGramos, int numeroFactura, int precioPorMayor,
            int cantidad, int montoTotal, double montoTotalConIva) {

        super(tipodeAlimento, contenidoGramos, numeroFactura);
        this.precioPorMayor = precioPorMayor;
        this.cantidad = cantidad;
        this.montoTotal = montoTotal;
        this.montoTotalConIva = montoTotalConIva;
    }

    public int getPrecioAlPorMayor() {
        return precioPorMayor;
    }

    public void setPrecioAlPorMayor(int precioAlPorMayor) {
        this.precioPorMayor = precioAlPorMayor;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(int montoTotal) {
        this.montoTotal = montoTotal;
    }

    public double getMontoTotalConIva() {
        return montoTotalConIva;
    }

    public void setMontoTotalConIva(double montoTotalConIva) {
        this.montoTotalConIva = montoTotalConIva;
    }

}
