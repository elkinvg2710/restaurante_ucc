/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronprototype;

import java.util.ArrayList;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class ClientePrototype {

    public static void main(String args[]) throws CloneNotSupportedException {

        int capacidadReporteFacturas = 12;

        // FacturaVentaDetal
        PrototipoFacturaPlato prototipoFacturaVentaDetal = new FacturaVentaDetal("PROTEINAS", 250, 34, 1123, 23000, 23000);

        // FacturaAlPorMayor
        PrototipoFacturaPlato prototipoFacturaVentaPorMayor = new FacturaVentaPorMayor("VIRAMINAS", 300, 23, 345, 23, 18000, 17);

        // FacturaVentaSinIVA
        PrototipoFacturaPlato prototipoFacturaVentaSinIva = new FacturaVentaSinIva("CEREALES", 200, 45, 10000, 9, 15000);

        ArrayList listaFacturasPlatos
                
                
                = new ArrayList();
        for (int i = 0; i < capacidadReporteFacturas; i++) {

            PrototipoFacturaPlato facturaVentaDetal = (PrototipoFacturaPlato) prototipoFacturaVentaDetal.Clone();
            facturaVentaDetal.setNumeroFactura(i * 3);

            System.out.println(facturaVentaDetal.getTipodeAlimento() + " / " + facturaVentaDetal.getContenidoGramos() + " / "
                    + facturaVentaDetal.getNumeroFactura());

            PrototipoFacturaPlato FacturaVentaPorMayor = (PrototipoFacturaPlato) prototipoFacturaVentaPorMayor.Clone();
            FacturaVentaPorMayor.setNumeroFactura(i * 4);

            System.out.println(FacturaVentaPorMayor.getTipodeAlimento() + " / " + FacturaVentaPorMayor.getContenidoGramos() + " / "
                    + FacturaVentaPorMayor.getNumeroFactura());

            PrototipoFacturaPlato FacturaVentaSinIva = (PrototipoFacturaPlato) prototipoFacturaVentaSinIva.Clone();
            FacturaVentaSinIva.setNumeroFactura(i * 5);

            System.out.println(FacturaVentaSinIva.getTipodeAlimento() + " / " + FacturaVentaSinIva.getContenidoGramos() + " / "
                    + FacturaVentaSinIva.getNumeroFactura());

        }
    }
}
