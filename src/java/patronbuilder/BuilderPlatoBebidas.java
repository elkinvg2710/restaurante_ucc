
package patronbuilder;


public class BuilderPlatoBebidas extends PlatoBuilder{
    //implementar los metodos
    
    public void construirPlato(int id, String name, String description, int price, int category){
        platoB.setId(id);
        platoB.setName(name);
        platoB.setDescription(description);
        platoB.setPrice(price);
       //en category especifico el id de la categoria, para bebidas= 4 
        platoB.setCategory(4);
    }
}
