
package patronbuilder;

import pojos.Plato;


public class PlatoDirector {
    
    //definimos una referencia al builder
    
    private PlatoBuilder platoBuilder;
    
    //metodo para construir peliculas
    
    public void BuildPlato (int id, String name, String desciption, int price, int category){
        platoBuilder.crearPlatoPatronBuiler();
        platoBuilder.construirPlato(id, name, desciption, price, category);
    
    }
    
    //ESTE ES EL CORAZON DEL PATRON BULDER
    public void setPlatoBuilder(PlatoBuilder plato){
        platoBuilder = plato;
    
    }
    
    //proveer un metodo para retornar el producto complejo que se ha construido
    public Plato getPlatoPatronBuilder(){
        return platoBuilder.getPlatoPatronBuiler();
    }
    
}
