
package patronbuilder;

import cruds.CRUDPlatos;
import java.util.LinkedList;
import pojos.Plato;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class TestProductoBuilder {
    private static CRUDPlatos crud = new CRUDPlatos();
    public  static void main(String[] args){   
        System.out.println("BUILDER -> PLATO ENTRADAS >>>");
        LinkedList<Plato> listaPlatoEntradas = crud.listarPlatosBuilder(1);
        for(int x = 0; x < listaPlatoEntradas.size(); x++){
            System.out.println("_______________________________");
            System.out.println("nombre > " + listaPlatoEntradas.get(x).getName() + " / precio > " + 
                    listaPlatoEntradas.get(x).getPrice());
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("BUILDER -> PLATO CARNES  >>> ");
        LinkedList<Plato> listaPlatoCarnes = crud.listarPlatosBuilder(2);
        for(int y = 0; y < listaPlatoCarnes.size(); y++){
            System.out.println("_______________________________");
            System.out.println("nombre > " + listaPlatoCarnes.get(y).getName() + " / precio > " + 
                    listaPlatoCarnes.get(y).getPrice());
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("BUILDER -> PLATO ACOMPAÑAMIENTOS");
        LinkedList<Plato> listaPlatoAcompañamientos = crud.listarPlatosBuilder(3);
        for(int z = 0; z < listaPlatoAcompañamientos.size(); z++){
            System.out.println("_______________________________");
            System.out.println("nombre > " + listaPlatoAcompañamientos.get(z).getName() + " / precio > " + 
                    listaPlatoAcompañamientos.get(z).getPrice());
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("BUILDER -> PLATO BEBIDAS");
        LinkedList<Plato> listaPlatoBebidas = crud.listarPlatosBuilder(4);
        for(int w = 0; w < listaPlatoBebidas.size(); w++){
            System.out.println("_______________________________");
            System.out.println("nombre > " + listaPlatoBebidas.get(w).getName() + " / precio > " + 
                    listaPlatoBebidas.get(w).getPrice());
        }
    } 
    
}
