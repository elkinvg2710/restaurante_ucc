
package patronbuilder;


public class BuilderPlatoCarnes extends PlatoBuilder{
    // debe implementar sus metodos abstractos
    
    public void construirPlato(int id,String name, String description, int price, int category){
        platoB.setId(id);
        platoB.setName(name);
        platoB.setDescription(description);
        platoB.setPrice(price);
       //en category especifico el id de a categoria, para accion=2
        platoB.setCategory(2);
    }
}
