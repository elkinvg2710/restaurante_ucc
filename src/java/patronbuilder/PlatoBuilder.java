/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronbuilder;

import pojos.Plato;

/**
 *
 * @author Elkin Valencia Gomez
 */
public abstract class PlatoBuilder {
    protected Plato platoB ;
    
    
    public Plato  getPlatoPatronBuiler(){
    return platoB;    
    }
    
    public void crearPlatoPatronBuiler(){
    platoB = new Plato(); 
    
    }
    
    public abstract void construirPlato (int id, String name, String description, int price, int category );
    
}
