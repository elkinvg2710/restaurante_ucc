/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patroniterator;

import connector.ConexionObjetos;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Category;

/**
 *
 * @author Elkin Valencia Gomez
 */
public class IteratorListaCategorias {
    // este objeto se encarga de gestionar los agregados a la coleccion
   
    private LinkedList<Category> listaCategorias = new LinkedList<Category>();
    private int size = 0;
    private static ConexionObjetos conn;
    
    //configuramos la instancia de singleton  desde el contructor
    public IteratorListaCategorias(){
        
        try {
            conn = ConexionObjetos.getInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public void add (int id){
        Category categoria = new Category();
        categoria.setId(id);
        
        try{
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM category WHERE "
                +  "id = '" + categoria.getId() + "'");
            while (rs.next()){
                categoria.setName(rs.getString("name"));
            }
            listaCategorias.add(categoria);
            rs.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    // debe proveer un metodo que retorne la lista a quien la requiera
    
    public CategoriasIterator iterator(){
        return new CategoriasIterator(listaCategorias);
    }
}
