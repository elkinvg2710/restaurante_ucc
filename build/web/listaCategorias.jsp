

<%@page import="patronfacade.PlatosFacade"%>
<%@page import="patronfacade.FacadePlatos"%>
<%@page import="pojos.Category"%>
<%@page import="cruds.CRUDCategorias"%>
<%@page import="pojos.Plato"%>
<%@page import="java.util.LinkedList"%>
<%@page import="cruds.CRUDPlatos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista Categorias</title>
    </head>
    <div align="center"><img src="descarga.jpg" alt="Img Vivero" style="width:400px;height:200px;border: 7px solid; color: azure"></div>
   <center><fieldset style="width:1100px"><legend><h1 style="color:blue;">RESTAURANTE</h1></legend>
           
    <body>
        <h1 style="color:blue;" >Lista Categorias Platos </h1>
        <br>
        <table border ="1">
            <tr>
                <td> LISTA PLATOS CATEGORIA ACOMPAÑAMIENTO >>> PATRON BUILDER
              
                    <table border ="1">
                        <tr>
                            <colgroup span="5" style="background: mediumaquamarine"></colgroup>
                            <td>Codigo plato</td>
                            <td>Nombre</td>
                            <td>Descripcion</td> 
                            <td>Precio</td>
                            <td>Categoria</td>
                        </tr>
                        <%
                            CRUDPlatos crudPlatos = new CRUDPlatos();
                            LinkedList<Plato> listaPatosAcompañamientos = crudPlatos.listarPlatosBuilder(3);
                            for (int i = 0; i < listaPatosAcompañamientos.size(); i++) {
                                out.println("<tr>");
                                out.println("<td>" + listaPatosAcompañamientos.get(i).getId() + "</td>");
                                out.println("<td>" + listaPatosAcompañamientos.get(i).getName() + "</td>");
                                out.println("<td>" + listaPatosAcompañamientos.get(i).getDescription() + "</td>");
                                out.println("<td>" + listaPatosAcompañamientos.get(i).getPrice() + "</td>");
                                out.println("<td>" + listaPatosAcompañamientos.get(i).getCategory() + "</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>
                </td>
            </tr>
            <tr>
                <td> LISTA PLATOS CATEGORIA BEBIDAS >>> PATRON BUILDER
                    <table border ="1">
                        <tr>  
                            <colgroup span="5" style="background: mediumaquamarine"></colgroup>
                            <td>codigo plato</td>
                            <td>nombre</td>
                            <td>descripcion</td> 
                            <td>precio</td>
                            <td>categoria</td>
                        </tr>
                        <%
                            LinkedList<Plato> listaPlatosBebidas = crudPlatos.listarPlatosBuilder(4);
                            for (int x = 0; x < listaPlatosBebidas.size(); x++) {
                                out.println("<tr>");
                                out.println("<td>" + listaPlatosBebidas.get(x).getId() + "</td>");
                                out.println("<td>" + listaPlatosBebidas.get(x).getName() + "</td>");
                                out.println("<td>" + listaPlatosBebidas.get(x).getDescription() + "</td>");
                                out.println("<td>" + listaPlatosBebidas.get(x).getPrice() + "</td>");
                                out.println("<td>" + listaPlatosBebidas.get(x).getCategory() + "</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>
                </td>
            </tr>
            <tr>
                <td>LISTA PLATOS CATEGORIA CARNES >>> PATRON BUILDER
                    <table border ="1">
                        <tr>
                           <colgroup span="5" style="background: mediumaquamarine"></colgroup>
                            <td>Codigo plato</td>
                            <td>Nombre</td>
                            <td>Descripcion</td> 
                            <td>Precio</td>
                            <td>Categoria</td>
                        </tr>
                        <%
                            LinkedList<Plato> listaPlatosCarnes = crudPlatos.listarPlatosBuilder(2);
                            for (int y = 0; y < listaPlatosCarnes.size(); y++) {
                                out.println("<tr>");
                                out.println("<td>" + listaPlatosCarnes.get(y).getId() + "</td>");
                                out.println("<td>" + listaPlatosCarnes.get(y).getName() + "</td>");
                                out.println("<td>" + listaPlatosCarnes.get(y).getDescription() + "</td>");
                                out.println("<td>" + listaPlatosCarnes.get(y).getPrice() + "</td>");
                                out.println("<td>" + listaPlatosCarnes.get(y).getCategory() + "</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>   
                </td>
            </tr>
            <tr>
                <td>LISTA PLATOS CATEGORIA ENTRADAS >>> PATRON BUILDER
                    <table border ="1">
                        <tr>
                           <colgroup span="5" style="background: mediumaquamarine"></colgroup>
                            <td>Codigo plato</td>
                            <td>Nombre</td>
                            <td>Descripcion</td> 
                            <td>Precio</td>
                            <td>Categoria</td>
                        </tr>
                        <%
                            LinkedList<Plato> listaPlatosEntradas = crudPlatos.listarPlatosBuilder(1);
                            for (int z = 0; z < listaPlatosEntradas.size(); z++) {
                                out.println("<tr>");
                                out.println("<td>" + listaPlatosEntradas.get(z).getId() + "</td>");
                                out.println("<td>" + listaPlatosEntradas.get(z).getName() + "</td>");
                                out.println("<td>" + listaPlatosEntradas.get(z).getDescription() + "</td>");
                                out.println("<td>" + listaPlatosEntradas.get(z).getPrice() + "</td>");
                                out.println("<td>" + listaPlatosEntradas.get(z).getCategory() + "</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>   
                </td>
            </tr>
            <tr>
                <td>LISTA PLATOS >>> PATRON FACADE
                
                    <table border ="1">
                        <tr>
                           <colgroup span="7" style="background: mediumaquamarine"></colgroup>
                            <td>Codigo plato</td>
                            <td>Nombre</td>
                            <td>Descripcion</td> 
                            <td>Precio</td>
                            <td>Nombre categoria</td>
                        </tr>
                        <%
                             CRUDCategorias crudCategorias = new CRUDCategorias();
                            LinkedList<Plato> listaPlatos = crudPlatos.listarPlatos();
                            LinkedList<Category> listaCategorias = crudCategorias.listarCategorias();
                           

                            for (int x = 0; x < listaPlatos.size(); x++) {
                                for (int y = 0; y < listaCategorias.size(); y++) {
                                    if (listaPlatos.get(x).getCategory() == listaCategorias.get(y).getId()) {
                                        listaPlatos.get(x).setCategoryName(listaCategorias.get(y).getName());
                                        Plato plato = listaPlatos.get(x);
                                        Category categoria = listaCategorias.get(y);
                                        out.println("<tr>");
                                        out.println("<td>" + plato.getId() + "</td>");
                                        out.println("<td>" + plato.getName() + "</td>");
                                        out.println("<td>" + plato.getDescription() + "</td>");
                                        out.println("<td>" + plato.getPrice() + "</td>");
                                        out.println("<td>" + categoria.getName() + "</td>");
                                        out.println("</tr>");
                                    }
                                }
                            }


                        %>
                    </table>   
                </td>
            </tr>
        </table>
        <br>
        <hr>
        <a href="platos.jsp" style="color:blue;  width: 170px; height: 30px" >Volver al inicio</a>
        <hr>
    </body>
</html>
