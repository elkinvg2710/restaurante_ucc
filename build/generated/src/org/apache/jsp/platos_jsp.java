package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.Iterator;
import pojos.Category;
import patroniterator.IteratorListaCategorias;
import cruds.CRUDCategorias;
import java.util.LinkedList;
import pojos.Plato;
import cruds.CRUDPlatos;

public final class platos_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Sistema de Restaurante</title>\n");
      out.write("    </head>\n");
      out.write("   <div align=\"center\"><img src=\"fondo.jpg\" alt=\"Img Vivero\" style=\"width:400px;height:200px;border: 7px solid; color: azure\"></div>\n");
      out.write("   <center><fieldset style=\"width:1100px\"><legend><h1 style=\"color:blue;\">RESTAURANTE</h1></legend>\n");
      out.write("    \n");
      out.write("           <h1 style=\"color:blue;\" > Platos Disponibles  </h1>\n");
      out.write("        <table border = \"1\">\n");
      out.write("            <tr>\n");
      out.write("                <colgroup span=\"5\" style=\"background: honeydew\"></colgroup>\n");
      out.write("             \n");
      out.write("                <td>Identificativo</td>\n");
      out.write("                <td>Nombre Platos</td>\n");
      out.write("                <td>Descripción</td>\n");
      out.write("                <td>Precio</td>\n");
      out.write("                <td>Categoria</td>\n");
      out.write("            </tr>\n");
      out.write("            ");

                CRUDPlatos crudPlatos = new CRUDPlatos();
                LinkedList<Plato> lista = crudPlatos.listarPlatos();
                for (int i = 0; i < lista.size(); i++) {
                    Plato plato = lista.get(i);
                    out.println("<tr>");
                    out.println("<td>" + plato.getId() + "</td>");
                    out.println("<td>" + plato.getName() + "</td>");
                    out.println("<td>" + plato.getDescription() + "</td>");
                    out.println("<td>" + plato.getPrice() + "</td>");
                    out.println("<td>" + plato.getCategory() + "</td>");
                    out.println("</tr>");
                }

            
      out.write("\n");
      out.write("        </table>\n");
      out.write("        <br>\n");
      out.write("        <a href=\"listaCategorias.jsp\">Lista de Platos por Categorias<a/>\n");
      out.write("        <hr>\n");
      out.write("        <br>\n");
      out.write("        <form name=\"formularioplatos\" action=\"ControladorPlatos\" method=\"get\">\n");
      out.write("            Codigo > <input type=\"text\" name=\"id\"><br>\n");
      out.write("            Nombre Plato > <input type=\"text\" name=\"name\"><br>\n");
      out.write("            Descripción > <input type=\"text\" name=\"description\"><br>\n");
      out.write("            Precio > <input type=\"txt\" name=\"price\" value=\"0\"><br>\n");
      out.write("            patron iterator >>> <br>\n");
      out.write("            categoria > \n");
      out.write("            <select name =\"category\">\n");
      out.write("                ");

                    CRUDCategorias crudCategorias = new CRUDCategorias();
                    IteratorListaCategorias listaCategorias = new IteratorListaCategorias();
                    LinkedList<Category> listaCat = crudCategorias.listarCategorias();
                    
                        for(int x = 0; x < listaCat.size(); x++){
                            listaCategorias.add(listaCat.get(x).getId());
                        }
                        Iterator<Category> iteratorCategorias = listaCategorias.iterator();
                        while(iteratorCategorias.hasNext()){
                            Category categoria = (Category)iteratorCategorias.next();
                            out.println( "<option value =\"" + categoria.getId() + "\">" + 
                                    categoria.getName() + "</option>");
                        }
                    
                    
      out.write("\n");
      out.write("                \n");
      out.write("            </select>\n");
      out.write("            \n");
      out.write("            \n");
      out.write("            <hr>\n");
      out.write("            <br>\n");
      out.write("            <input type=\"submit\" name=\"insertarplato\" value=\"Guardar Plato\">\n");
      out.write("            <input type=\"submit\" name=\"editarplato\" value=\"Actualizar Plato\">\n");
      out.write("            <input type=\"submit\" name=\"eliminarplato\" value=\"Borrar Plato\">\n");
      out.write("        </form>\n");
      out.write("        <br>\n");
      out.write("        <hr>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
