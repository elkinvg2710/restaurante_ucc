<%-- 
    Document   : test.jsp
    Created on : 4/11/2020, 03:19:07 PM
    Author     : Elkin Valencia Gomez
--%>

<%@page language = "java" %>
<%@page  import = "java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hola Mundo --> Este es mi JSP </h1>
        
        <%
            out.println("<hr><br>");
            out.println("<h2> La hora actua es --->" + new Date().toString() + " Saludos </h2>");
            out.println("<br><hr>");
            
        
        %>
        
    </body>
</html>
