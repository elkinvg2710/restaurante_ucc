

<%@page import="java.util.Iterator"%>
<%@page import="pojos.Category"%>
<%@page import="patroniterator.IteratorListaCategorias"%>
<%@page import="cruds.CRUDCategorias"%>
<%@page import="java.util.LinkedList"%>
<%@page import="pojos.Plato"%>
<%@page import="cruds.CRUDPlatos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Restaurante</title>
    </head>
    
   <div align="center"><img src="fondo.jpg" alt="Img Plato" style="width:400px;height:200px;border: 7px solid; color: azure"></div>
   <center><fieldset style="width:1100px"><legend><h1 style="color:blue;">RESTAURANTE</h1></legend>
    
           <h1 style="color:blue;" > Platos Disponibles  </h1>
        <table border = "1">
            <tr>
                <colgroup span="5" style="background: mediumaquamarine"></colgroup>
             
                <td>Identificativo</td>
                <td>Nombre Platos</td>
                <td>Descripción</td>
                <td>Precio</td>
                <td>Categoria</td>
            </tr>
            <%
                CRUDPlatos crudPlatos = new CRUDPlatos();
                LinkedList<Plato> lista = crudPlatos.listarPlatos();
                for (int i = 0; i < lista.size(); i++) {
                    Plato plato = lista.get(i);
                    out.println("<tr>");
                    out.println("<td>" + plato.getId() + "</td>");
                    out.println("<td>" + plato.getName() + "</td>");
                    out.println("<td>" + plato.getDescription() + "</td>");
                    out.println("<td>" + plato.getPrice() + "</td>");
                    out.println("<td>" + plato.getCategory() + "</td>");
                    out.println("</tr>");
                }

            %>
        </table>
        <br>
        <a href="listaCategorias.jsp" style="color:blue;  width: 170px; height: 30px" > Lista de Platos por Categorias<a/>
        <hr>
        <br>
        <form name="formularioplatos" action="ControladorPlatos" method="get">
            CODIGO > <input type="text" name="id"><br>
            NOMBRE DEL PLATO > <input type="text" name="name"><br>
            DESCRIPCION > <input type="text" name="description"><br>
            PRECIO > <input type="txt" name="price" value="0"><br>
            PATRON ITERATOR --> <br>
            CATEGORIA > 
            <select name ="category">
                <%
                    CRUDCategorias crudCategorias = new CRUDCategorias();
                    IteratorListaCategorias listaCategorias = new IteratorListaCategorias();
                    LinkedList<Category> listaCat = crudCategorias.listarCategorias();
                    
                        for(int x = 0; x < listaCat.size(); x++){
                            listaCategorias.add(listaCat.get(x).getId());
                        }
                        Iterator<Category> iteratorCategorias = listaCategorias.iterator();
                        while(iteratorCategorias.hasNext()){
                            Category categoria = (Category)iteratorCategorias.next();
                            out.println( "<option value =\"" + categoria.getId() + "\">" + 
                                    categoria.getName() + "</option>");
                        }
                    
                    %>
                
            </select>
            
            
            <hr>
            <br>
            <input type="submit" name="insertarplato" value="Guardar Plato" style="color: blue; border: 1px solid; width: 150px; height: 30px">
            <input type="submit" name="editarplato" value="Actualizar Plato" style="color: blue; border: 1px solid; width: 170px; height: 30px">
            <input type="submit" name="eliminarplato" value="Borrar Plato" style="color:blue; border: 1px solid; width: 170px; height: 30px">
        </form>
        <br>
        <hr>
    </body>
</html>
